#include "Corrections/EGM/interface/eleCorrectionsInterface.h"

eleCorrections::eleCorrections(std::string filename, std::string corrname) : 
  elecorr(filename, corrname) {}

// Destructor
eleCorrections::~eleCorrections() {}

fRVec eleCorrections::get_ele_sf(std::string corrname, std::string syst,
    std::string wp, fRVec eta, fRVec pt)
{
    fRVec sf;
    for (size_t i = 0; i < pt.size(); i++) {
        if (pt[i] < 10.) sf.push_back(1.);
        else {
            // for the reco SFs need to actively disambiguarte between pt ranges amd years
            // plus different schemes of combination can be used. This if/else takes care of all this
            // cf. https://twiki.cern.ch/twiki/bin/view/CMS/EgammSFandSSRun3?rev=35#Electron_and_Electron_HLT_JSON_f
            if (wp.find("Reco") != std::string::npos) {
                // using "reco" the three SF get put together and also their systematics
                if (wp == "Reco") {
                    if (pt[i] < 20.)
                        sf.push_back(elecorr.eval({corrname, syst, "RecoBelow20", eta[i], pt[i]}));
                    else if (pt[i] > 20. && pt[i] < 75.) {
                        if (corrname.find("2022") != std::string::npos)
                            sf.push_back(elecorr.eval({corrname, syst, "Reco20to75", eta[i], pt[i]}));

                        else if (corrname.find("2016") != std::string::npos || corrname.find("2017") != std::string::npos || corrname.find("2018") != std::string::npos)
                            sf.push_back(elecorr.eval({corrname, syst, "RecoAbove20", eta[i], pt[i]}));

                        else
                            sf.push_back(1.);
                    }
                    else if (pt[i] > 75.) {
                        if (corrname.find("2022") != std::string::npos)
                            sf.push_back(elecorr.eval({corrname, syst, "RecoAbove75", eta[i], pt[i]}));

                        else if (corrname.find("2016") != std::string::npos || corrname.find("2017") != std::string::npos || corrname.find("2018") != std::string::npos)
                            sf.push_back(elecorr.eval({corrname, syst, "RecoAbove20", eta[i], pt[i]}));

                        else
                            sf.push_back(1.);
                    }
                    else
                        sf.push_back(1.);
                }
                // using "RecoAbove20" the two SF >20GeV get put together and also their systematics
                else if (wp == "RecoAbove20") {
                    if (pt[i] > 20. && pt[i] < 75.) {
                        if (corrname.find("2022") != std::string::npos)
                            sf.push_back(elecorr.eval({corrname, syst, "Reco20to75", eta[i], pt[i]}));

                        else if (corrname.find("2016") != std::string::npos || corrname.find("2017") != std::string::npos || corrname.find("2018") != std::string::npos)
                            sf.push_back(elecorr.eval({corrname, syst, "RecoAbove20", eta[i], pt[i]}));

                        else
                            sf.push_back(1.);
                    }
                    else if (pt[i] > 75.) {
                        if (corrname.find("2022") != std::string::npos)
                            sf.push_back(elecorr.eval({corrname, syst, "RecoAbove75", eta[i], pt[i]}));

                        else if (corrname.find("2016") != std::string::npos || corrname.find("2017") != std::string::npos || corrname.find("2018") != std::string::npos)
                            sf.push_back(elecorr.eval({corrname, syst, "RecoAbove20", eta[i], pt[i]}));

                        else
                            sf.push_back(1.);
                    }
                    else
                        sf.push_back(1.);
                }
                else if (wp == "RecoBelow20") {
                    if (pt[i] < 20.)
                        sf.push_back(elecorr.eval({corrname, syst, wp, eta[i], pt[i]}));
                    else
                        sf.push_back(1.);
                }
                else if (wp == "Reco20to75") {
                    if (pt[i] > 20. && pt[i] < 75.)
                        sf.push_back(elecorr.eval({corrname, syst, wp, eta[i], pt[i]}));
                    else
                        sf.push_back(1.);
                }
                else if (wp == "RecoAbove75") {
                    if (pt[i] > 75.)
                        sf.push_back(elecorr.eval({corrname, syst, wp, eta[i], pt[i]}));
                    else
                        sf.push_back(1.);
                }
            }
            // this else takes care of all cases where wp is not a reco sf
            else {
                sf.push_back(elecorr.eval({corrname, syst, wp, eta[i], pt[i]}));
            }
        }
    }
    return sf;
}

fRVec eleCorrections::get_ele_sf(std::string corrname, std::string syst,
    std::string wp, fRVec eta, fRVec pt, fRVec phi)
{
    fRVec sf;
    for (size_t i = 0; i < pt.size(); i++) {
        if (pt[i] < 10.) sf.push_back(1.);
        else {
            // for the reco SFs need to actively disambiguarte between pt ranges while storing the SFs in a single variable
            // cf. https://twiki.cern.ch/twiki/bin/view/CMS/EgammSFandSSRun3?rev=35#Electron_and_Electron_HLT_JSON_f
            if (wp.find("Reco") != std::string::npos) {
                // using "reco" the three SF get put together and also their systematics
                if (wp == "Reco") {
                    if (pt[i] < 20.)
                        sf.push_back(elecorr.eval({corrname, syst, "RecoBelow20", eta[i], pt[i], phi[i]}));
                    else if (pt[i] > 20. && pt[i] < 75.)
                        sf.push_back(elecorr.eval({corrname, syst, "Reco20to75", eta[i], pt[i], phi[i]}));
                    else if (pt[i] > 75.)
                        sf.push_back(elecorr.eval({corrname, syst, "RecoAbove75", eta[i], pt[i], phi[i]}));
                    else
                        sf.push_back(1.);
                }
                // using "RecoAbove20" the two SF >20GeV get put together and also their systematics
                else if (wp == "RecoAbove20") {
                    if (pt[i] > 20. && pt[i] < 75.)
                        sf.push_back(elecorr.eval({corrname, syst, "Reco20to75", eta[i], pt[i], phi[i]}));
                    else if (pt[i] > 75.)
                        sf.push_back(elecorr.eval({corrname, syst, "RecoAbove75", eta[i], pt[i], phi[i]}));
                    else
                        sf.push_back(1.);
                }
                else if (wp == "RecoBelow20") {
                    if (pt[i] < 20.)
                        sf.push_back(elecorr.eval({corrname, syst, wp, eta[i], pt[i], phi[i]}));
                    else
                        sf.push_back(1.);
                }
                else if (wp == "Reco20to75") {
                    if (pt[i] > 20. && pt[i] < 75.)
                        sf.push_back(elecorr.eval({corrname, syst, wp, eta[i], pt[i], phi[i]}));
                    else
                        sf.push_back(1.);
                }
                else if (wp == "RecoAbove75") {
                    if (pt[i] > 75.)
                        sf.push_back(elecorr.eval({corrname, syst, wp, eta[i], pt[i], phi[i]}));
                    else
                        sf.push_back(1.);
                }
            }
            // this else takes care of all cases where wp is not a reco sf
            else {
                sf.push_back(elecorr.eval({corrname, syst, wp, eta[i], pt[i], phi[i]}));
            }
        }
    }
    return sf;
}

fRVec eleCorrections::get_ele_ss(Double_t run, 
    iRVec gain, fRVec eta, fRVec r9, fRVec pt) 
{
    fRVec sf;
    for (size_t i = 0; i < pt.size(); i++) {
        if (pt[i] < 10.) sf.push_back(1.);
        else {
            sf.push_back(elecorr.eval({"total_correction", gain[i], run, eta[i], r9[i], pt[i]}));
        }
    }
    return sf;
}

fRVec eleCorrections::get_ele_scale_unc(Double_t run, std::string syst,
    iRVec gain, fRVec eta, fRVec r9, fRVec pt)
{
    fRVec sf;
    for (size_t i = 0; i < pt.size(); i++) {
        if (pt[i] < 10.) sf.push_back(1.);
        else {
            if (syst == "down") {
                sf.push_back(1. - elecorr.eval({"total_uncertainty", gain[i], 
                                                run, eta[i], r9[i], pt[i]}));
            }
            else {
                sf.push_back(1. + elecorr.eval({"total_uncertainty", gain[i],
                                                run, eta[i], r9[i], pt[i]}));
            }
        }
    }
    return sf;
}

egmSSOutput eleCorrections::get_ele_ss(int event, std::string syst,
    fRVec eta, fRVec r9, fRVec pt)
{
    ui64RVec smearseed;
    fRVec sf;
    for (size_t i = 0; i < pt.size(); i++) {
        if (pt[i] < 10.) sf.push_back(1.);
        else {
            // for reproducibility, re-initialise seed for each ele based on inputs
            uint64_t seed = event * 100 + uint64_t(pt[i] * 100)
                          + uint64_t(std::abs(eta[i]) * 100) * 100
                          + uint64_t(std::abs(r9[i]) * 100) * 10000;
            rndm.SetSeed(seed);
            smearseed.push_back(seed);
            
            if (syst == "down") {
                Double_t smearing = elecorr.eval({"rho", eta[i], r9[i]});
                Double_t uncrtnty = elecorr.eval({"err_rho", eta[i], r9[i]});
                sf.push_back(rndm.Gaus(1., smearing - uncrtnty));
            }
            else if (syst == "up") {
                Double_t smearing = elecorr.eval({"rho", eta[i], r9[i]});
                Double_t uncrtnty = elecorr.eval({"err_rho", eta[i], r9[i]});
                sf.push_back(rndm.Gaus(1., smearing + uncrtnty));
            }
            else {
                Double_t smearing = elecorr.eval({"rho", eta[i], r9[i]});
                sf.push_back(rndm.Gaus(1., smearing));
            }
        }
    }

    egmSSOutput out({smearseed, sf});

    return out;
}

egmSSOutput eleCorrections::get_ele_synched_ss(uint64_t event_seed, std::string syst,
    fRVec eta, fRVec r9, fRVec pt)
{
    ui64RVec smearseed;
    fRVec sf;
    for (size_t i = 0; i < pt.size(); i++) {
        if (pt[i] < 10.) sf.push_back(1.);
        else {
            uint64_t seed = RNGseed.object_seed(event_seed, i, 60);
            smearseed.push_back(seed);

            if (syst == "down") {
                Double_t smearing = elecorr.eval({"rho", eta[i], r9[i]});
                Double_t uncrtnty = elecorr.eval({"err_rho", eta[i], r9[i]});
                sf.push_back(RNGnumpy.normal(2, seed) * (smearing - uncrtnty) + 1.);
            }
            else if (syst == "up") {
                Double_t smearing = elecorr.eval({"rho", eta[i], r9[i]});
                Double_t uncrtnty = elecorr.eval({"err_rho", eta[i], r9[i]});
                sf.push_back(RNGnumpy.normal(1, seed) * (smearing + uncrtnty) + 1.);
            }
            else {
                Double_t smearing = elecorr.eval({"rho", eta[i], r9[i]});
                sf.push_back(RNGnumpy.normal(0, seed) * smearing + 1.);
            }
        }
    }

    egmSSOutput out({smearseed, sf});

    return out;
}
